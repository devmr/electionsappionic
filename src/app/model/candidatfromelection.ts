import { Candidat } from './candidat';

export interface Candidatfromelection {
    candidat_count: number;
    candidats: Candidat[];
}