import { NumeroCandidat } from './numerocandidat';
import { PhotoCandidat } from './photocandidat';

export interface Candidat {
    name: string;
    name2: string;
    firstname: string;
    numeros: NumeroCandidat[];
    photos: PhotoCandidat[];
}