
export interface Election {
    id: number;
    name: string;
    encours: boolean;
    slug: string;
    officialdate: string;
}