import {Component, OnInit, Input, Inject} from '@angular/core';

@Component({
  selector: 'app-candidat',
  templateUrl: './candidat.component.html',
  styleUrls: ['./candidat.component.scss'],
})
export class CandidatComponent implements OnInit {

  @Input() Candidats: Array<{ content: string; }>;

  constructor(@Inject('globalParam') private globalParam) { }

  ngOnInit() {}

}
