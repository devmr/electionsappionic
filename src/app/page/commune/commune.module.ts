import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CommunePage } from './commune.page';
import {TranslateModule} from "@ngx-translate/core";
const routes: Routes = [
  {
    path: '',
    component: CommunePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
      TranslateModule
  ],
  declarations: [CommunePage]
})
export class CommunePageModule {}
