import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from '../../service/language.service';
import { Storage } from '@ionic/storage';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

const LNG_KEY = 'SELECTED_LANGUAGE';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

    langs: any[] = [];
    defaultLang: string = 'fr';
    storageform: FormGroup;

  constructor(
      private langservice: LanguageService,
      private translate: TranslateService,
      private storage: Storage,
      private formBuilder: FormBuilder) {

      this.storageform = this.formBuilder.group({
          lang: ['', Validators.required]
      });
  }

  ngOnInit() {
      this.storage.get(LNG_KEY).then((val) => {
          console.log(val);
          this.storageform.controls['lang'].setValue(val)
      });
    this.langs = this.langservice.getLanguages();
  }

    submitForm() {
        console.log(this.storageform.value);
        this.storage.set(LNG_KEY,this.storageform.value.lang )
    }


}
