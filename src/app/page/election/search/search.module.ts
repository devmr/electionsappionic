import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SearchPage } from './search.page';
import {TranslateModule} from "@ngx-translate/core";

import { CandidatComponent } from '../../../component/candidat/candidat.component';

const routes: Routes = [
  {
    path: '',
    component: SearchPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
      TranslateModule
  ],
  entryComponents: [CandidatComponent],
  declarations: [SearchPage, CandidatComponent]
})
export class SearchPageModule {}
