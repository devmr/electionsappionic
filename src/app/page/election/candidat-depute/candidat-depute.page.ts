import {Component, Inject, OnInit} from '@angular/core';
import {CandidatdeputeService} from '../../../service/candidatdepute.service';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {DistrictService} from '../../../service/district.service';

@Component({
  selector: 'app-candidat-depute',
  templateUrl: './candidat-depute.page.html',
  styleUrls: ['./candidat-depute.page.scss'],
})
export class CandidatDeputePage implements OnInit {

  Candidats: any;
  placeItem:any;

  constructor(
      private translate: TranslateService,
      private route: ActivatedRoute,
      private candidatdeputeService: CandidatdeputeService,
      private districtService: DistrictService
  ) {

  }

  ngOnInit() {
    this.load();
  }

  load() {
    let electionId = this.route.snapshot.queryParamMap.get('election');
    let districtId = this.route.snapshot.queryParamMap.get('district');
    this.getInfoPlace(districtId);
    this.getCandidatsFromElection(electionId, districtId);
  }

  doRefresh(event) {
    this.load();
    event.target.complete();
  }

  getCandidatsFromElection(electionId: string, districtId: string) {
    this.candidatdeputeService.getCandidatsFromElection(electionId, districtId, '1').subscribe(data => {
      // console.log(data);
      this.Candidats = data ;
    });
  }

  getInfoPlace(districtId) {
    //this.districtService.getSingle(districtId);
    /*
    this.districtService.getSingle(districtId).subscribe(result => {
      this.placeItem = result;
    });
     */
  }



}
