import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import {TranslateModule} from "@ngx-translate/core";

import { CandidatDeputePage } from './candidat-depute.page';
import { CandidatComponent } from '../../../component/candidat/candidat.component';

const routes: Routes = [
  {
    path: '',
    component: CandidatDeputePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  entryComponents: [CandidatComponent],
  declarations: [CandidatDeputePage, CandidatComponent]
})
export class CandidatDeputePageModule {}
