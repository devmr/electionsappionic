import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatDeputePage } from './candidat-depute.page';

describe('CandidatDeputePage', () => {
  let component: CandidatDeputePage;
  let fixture: ComponentFixture<CandidatDeputePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatDeputePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatDeputePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
