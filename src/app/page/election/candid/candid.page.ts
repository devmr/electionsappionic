import {Component, Inject, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {ElectionService} from "../../../service/election.service";
import { TranslateService } from '@ngx-translate/core';
import {CandidatService} from "../../../service/candidat.service";

@Component({
  selector: 'app-candid',
  templateUrl: './candid.page.html',
  styleUrls: ['./candid.page.scss'],
})
export class CandidPage implements OnInit {

  item:any;
  itemsCandidats:any;
  electionId: string;

  constructor(
      private translate: TranslateService,
      private route: ActivatedRoute,
      private electionService: ElectionService,
      private candidatService: CandidatService,
      @Inject('globalParam') private globalParam
  ) { }

  ngOnInit() {
      this.load();
  }

  load() {
      this.electionId = this.route.snapshot.paramMap.get('id');
      this.getSingle(this.electionId);
      this.getCandidatsFromElection(this.electionId);
  }

  getSingle(id: string) {
      // Get the information from the API
      this.electionService.getSingle(id).subscribe(result => {
          this.item = result;
      });
  }

    getCandidatsFromElection(id: string) {
        this.candidatService.getCandidatsFromElection(id).subscribe(data => {
            console.log(data);
            this.itemsCandidats = data ;
        });
    }

    doRefresh(event) {
        this.load();
        event.target.complete();
    }



}
