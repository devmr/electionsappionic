import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidPage } from './candid.page';

describe('CandidPage', () => {
  let component: CandidPage;
  let fixture: ComponentFixture<CandidPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
