import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {ElectionService} from "../../../service/election.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-parti',
  templateUrl: './parti.page.html',
  styleUrls: ['./parti.page.scss'],
})
export class PartiPage implements OnInit {

    item:any;

  constructor(private translate: TranslateService, private route: ActivatedRoute,private electionService: ElectionService) { }

  ngOnInit() {
      let id: string = this.route.snapshot.paramMap.get('id');
      this.getSingle(id);
  }

  getSingle(id: string) {
      // Get the information from the API
      this.electionService.getSingle(id).subscribe(result => {
          this.item = result;
      });
  }

}
