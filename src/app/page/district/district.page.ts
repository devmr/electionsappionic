import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import {ElectionService} from "../../service/election.service";
import {DistrictService} from "../../service/district.service";
import {param} from "../../shared/param";

@Component({
  selector: 'app-district',
  templateUrl: './district.page.html',
  styleUrls: ['./district.page.scss'],
})
export class DistrictPage implements OnInit {

  items:any;
  electionId:any;
  electionItem:any;
  urlItem:string = '/commune';

  constructor(
      private electionService: ElectionService,
      private route: ActivatedRoute,
      private translate: TranslateService,
      private districtService: DistrictService
  ) { }

  ngOnInit() {
    this.load();
  }

  load() {
      this.electionId = this.route.snapshot.queryParamMap.get('election');
      let parentId = this.route.snapshot.queryParamMap.get('region');
      this.getSingleElection(this.electionId);
      this.getAll(parentId);
  }

  getAll(parentId) {
      this.districtService.getAll(parentId).subscribe(data => {
          console.log(data);
          this.items = data ;
      });
  }

  getSingleElection(id: string) {
      // Get the information from the API
      this.electionService.getSingle(id).subscribe(result => {
          this.electionItem = result;
      });
  }

    getUrl(item) {
      let url = this.urlItem;

      if (typeof item.electiontType == 'undefined') {
          return url;
      }

      console.log(item.electiontType.id + ' --- ' + param.legislativesID);

      if (item.electiontType.id == param.legislativesID) {
          url = '/candidat-depute/';
      }

      return url;
    }

  doRefresh(event) {
      this.load();
      event.target.complete();
  }

}
