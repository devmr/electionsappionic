import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistrictPage } from './district.page';

describe('DistrictPage', () => {
  let component: DistrictPage;
  let fixture: ComponentFixture<DistrictPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistrictPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistrictPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
