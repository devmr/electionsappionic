import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import {ElectionService} from "../../service/election.service";
import {RegionService} from "../../service/region.service";

@Component({
  selector: 'app-region',
  templateUrl: './region.page.html',
  styleUrls: ['./region.page.scss'],
})
export class RegionPage implements OnInit {

    items:any;
    electionId:any;
    electionItem:any;

  constructor(
      private electionService: ElectionService,
      private route: ActivatedRoute,
      private translate: TranslateService,
      private regionService: RegionService
  ) { }

  ngOnInit() {
      this.load();
  }

    load() {
        this.electionId = this.route.snapshot.queryParamMap.get('election');
        let parentId = this.route.snapshot.queryParamMap.get('province');
        this.getSingleElection(this.electionId);
        this.getAll(parentId);
    }

    getAll(parentId) {
        this.regionService.getAll(parentId).subscribe(data => {
            console.log(data);
            this.items = data ;
        });
    }

    getSingleElection(id: string) {
        // Get the information from the API
        this.electionService.getSingle(id).subscribe(result => {
            this.electionItem = result;
        });
    }

    doRefresh(event) {
        this.load();
        event.target.complete();
    }

}
