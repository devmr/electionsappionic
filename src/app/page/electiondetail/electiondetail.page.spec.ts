import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectiondetailPage } from './electiondetail.page';

describe('ElectiondetailPage', () => {
  let component: ElectiondetailPage;
  let fixture: ComponentFixture<ElectiondetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectiondetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectiondetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
