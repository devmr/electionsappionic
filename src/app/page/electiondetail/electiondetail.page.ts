import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {ElectionService} from "../../service/election.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-electiondetail',
  templateUrl: './electiondetail.page.html',
  styleUrls: ['./electiondetail.page.scss'],
})
export class ElectiondetailPage implements OnInit {

    item:any;
    id: string;

  constructor(private translate: TranslateService, private route: ActivatedRoute,private electionService: ElectionService) { }

  ngOnInit() {
      this.id = this.route.snapshot.paramMap.get('id');
      this.getSingle(this.id);
  }

  doRefresh(event) {
      this.getSingle(this.id);
      event.target.complete();
  }

  getSingle(id: string) {
      // Get the information from the API
      this.electionService.getSingle(id).subscribe(result => {
          this.item = result;
      });
  }

}
