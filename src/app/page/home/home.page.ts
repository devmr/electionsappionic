import { Component, OnInit } from '@angular/core';
import { ElectionService } from '../../service/election.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

    items = [];

    constructor(private electionService: ElectionService,
                private translate: TranslateService) {

    }

    ngOnInit() {
        this.getAll();
    }

    doRefresh(event) {
        this.getAll();
        event.target.complete();
    }

    getAll(): void {
        this.electionService.getAll().subscribe(data => {
            console.log(data);
            this.items = data ;
        });
    }
}
