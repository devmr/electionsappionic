import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {CandidatService} from "../../service/candidat.service";
import {param} from "../../shared/param";
import {Observable} from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  results: Observable<any>;
  search: string = '';
  pathImg: string = param.baseurl + 'media/cache/rc_270_200/assets/candidats/c_/';
  isSearch: boolean = false;


  constructor(
      private translate: TranslateService,
      private candidatService: CandidatService
  ) { }

  ngOnInit() {
  }

  searchChanged() {
        // Call our service function which returns an Observable
    this.results = this.candidatService.getCandidatsFromSearch(this.search);
    if (this.results) {
      this.disableSearch();
    }
  }

  enableSearch() {
    this.isSearch = true;
  }

  disableSearch() {
    this.isSearch = false;
  }
}
