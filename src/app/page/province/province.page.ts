import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {ProvinceService} from "../../service/province.service";
import { ActivatedRoute } from '@angular/router';
import {ElectionService} from "../../service/election.service";

@Component({
  selector: 'app-province',
  templateUrl: './province.page.html',
  styleUrls: ['./province.page.scss'],
})
export class ProvincePage implements OnInit {

  items:any;
  electionId:any;
  electionItem:any;

  constructor(
      private electionService: ElectionService,
      private route: ActivatedRoute,
      private translate: TranslateService,
      private provinceService: ProvinceService
  ) { }

  ngOnInit() {
      this.load();
  }

  load() {
      this.electionId = this.route.snapshot.queryParamMap.get('election');
      this.getSingleElection(this.electionId);
      this.getAll();
  }

  getAll() {
      this.provinceService.getAll().subscribe(data => {
          console.log(data);
          this.items = data ;
      });
  }

  getSingleElection(id: string) {
      // Get the information from the API
      this.electionService.getSingle(id).subscribe(result => {
          this.electionItem = result;
      });
  }

  doRefresh(event) {
      this.load();
      event.target.complete();
  }

}
