import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvincePage } from './province.page';

describe('ProvincePage', () => {
  let component: ProvincePage;
  let fixture: ComponentFixture<ProvincePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvincePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvincePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
