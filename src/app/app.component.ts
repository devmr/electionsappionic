import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

import {TranslateService} from '@ngx-translate/core';
import { LanguageService } from './service/language.service';

import { param } from './shared/param';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Elections',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Recherche',
      url: '/search',
      icon: 'search'
    },
    {
      title: 'A propos',
      url: '/about',
      icon: 'alert'
    },
      {
          title: 'Paramètres',
          url: '/settings',
          icon: 'settings'
      }

  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private ga: GoogleAnalytics,
    private translate: TranslateService,
    private languageService: LanguageService
  ) {

      this.initializeApp();
  }

  initializeApp() {

    this.platform.ready().then(() => {
      // this.statusBar.styleDefault();
      this.splashScreen.hide();

      /*console.log(param.baseurl)
      console.log(param.ga)*/

        this.languageService.setInitialAppLanguage();

        this.ga.startTrackerWithId(param.ga)
            .then(() => {
                console.log('Google analytics is ready now');
                this.ga.trackView('test');
                // Tracker is ready
                // You can now track pages or set additional information such as AppVersion or UserId
            })
            .catch(e => console.log('Error starting GoogleAnalytics', e));
        // let status bar overlay webview
        this.statusBar.overlaysWebView(false);

    });
  }
}
