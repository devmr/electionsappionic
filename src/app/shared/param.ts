enum ElectionType {
    presidentiellesID = 1,
    legislativesID = 2
};

export const param = {
    'baseurl' : 'https://elections-mg.com/',
    'ga' : 'UA-89654867-4',
    'legislativesID' : ElectionType.legislativesID,
    'presidentiellesID' : ElectionType.presidentiellesID

};

