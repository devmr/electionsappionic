import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './page/home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './page/list/list.module#ListPageModule'
  },
  {
    path: 'about',
    loadChildren: './page/about/about.module#AboutPageModule'
  },
  {
    path: 'settings',
    loadChildren: './page/settings/settings.module#SettingsPageModule'
  },
  {
    path: 'electiondetail/:id',
    loadChildren: './page/electiondetail/electiondetail.module#ElectiondetailPageModule'
  },
  { path: 'search', loadChildren: './page/search/search.module#SearchPageModule' },
  { path: 'election/candid/:id', loadChildren: './page/election/candid/candid.module#CandidPageModule' },
  { path: 'election/results/:id', loadChildren: './page/election/results/results.module#ResultsPageModule' },
  { path: 'election/search/:id', loadChildren: './page/election/search/search.module#SearchPageModule' },
  { path: 'election/parti/:id', loadChildren: './page/election/parti/parti.module#PartiPageModule' },
  { path: 'election/stats/:id', loadChildren: './page/election/stats/stats.module#StatsPageModule' },
  { path: 'province', loadChildren: './page/province/province.module#ProvincePageModule' },
  { path: 'region', loadChildren: './page/region/region.module#RegionPageModule' },
  { path: 'district', loadChildren: './page/district/district.module#DistrictPageModule' },
  { path: 'commune', loadChildren: './page/commune/commune.module#CommunePageModule' },
  { path: 'quartier', loadChildren: './page/quartier/quartier.module#QuartierPageModule' },  { path: 'candidat-depute', loadChildren: './page/election/candidat-depute/candidat-depute.module#CandidatDeputePageModule' }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
