import { Injectable } from '@angular/core';
import {param} from "../shared/param";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DistrictService {

    apiUrl:string = param.baseurl + 'api/district';

    constructor(private http: HttpClient) { }

    getAll(parentId) {
        let url = this.apiUrl + '?region=' + parentId
        console.log(url)
        return this.http.get(url);
    }

    getSingle(id: string) {
        return this.http.get(this.apiUrl + '/' + id);
    }
}
