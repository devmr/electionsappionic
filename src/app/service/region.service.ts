import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {param} from "../shared/param";

@Injectable({
  providedIn: 'root'
})
export class RegionService {

    apiUrl:string = param.baseurl + 'api/region';

  constructor(private http: HttpClient) { }

    getAll(parentId) {
        let url = this.apiUrl + '?province=' + parentId
        console.log(url)
        return this.http.get(url);
    }

}
