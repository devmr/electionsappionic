import { Injectable } from '@angular/core';
import {param} from "../shared/param";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ProvinceService {

    apiUrl:string = param.baseurl + 'api/province';

  constructor(private http: HttpClient) { }

  getAll() {
      console.log(this.apiUrl)
      return this.http.get(this.apiUrl);
  }
}
