import { Injectable } from '@angular/core';
import {param} from '../shared/param';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CandidatdeputeService {

  apiUrl:string = param.baseurl + 'api/candidat';

  constructor(private http: HttpClient) { }

  getCandidatsFromElection(electionId: string, districtId: string, titulaire: string) {
    console.log(this.apiUrl + '?electionsids=' + electionId + '&districts=' + districtId + '&titulaire=' + titulaire )
    return this.http.get(this.apiUrl + '?electionsids=' + electionId + '&districts=' + districtId + '&titulaire=' + titulaire );
  }
}
