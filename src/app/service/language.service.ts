import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { Storage } from '@ionic/storage';

const LNG_KEY = 'SELECTED_LANGUAGE';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  selected = '';

  constructor(private translate: TranslateService, private storage: Storage) { }

    setInitialAppLanguage() {
        let language = this.translate.getBrowserLang();
        this.translate.setDefaultLang(language);

        this.storage.get(LNG_KEY).then(val => {
            if (val) {
                console.log(val);
                this.setLanguage(val);
                this.selected = val;
            }
        });
    }

    getCurrentLangage(): string {
      var lg = '';
      this.storage.get(LNG_KEY).then(val => {
          if (val) {
              lg = val;
          }
      });
      return lg;
    }

    getLanguages() {
        return [
            { text: 'English', value: 'en' },
            { text: 'Français', value: 'fr' },
        ];
    }

    setLanguage(lng) {
        console.log(lng);
        this.translate.use(lng);
        this.selected = lng;
        this.storage.set(LNG_KEY, lng);
    }
}
