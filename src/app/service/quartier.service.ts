import { Injectable } from '@angular/core';
import {param} from "../shared/param";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class QuartierService {

    apiUrl:string = param.baseurl + 'api/quartier';

    constructor(private http: HttpClient) { }

    getAll() {
        console.log(this.apiUrl)
        return this.http.get(this.apiUrl);
    }
}
