import { TestBed } from '@angular/core/testing';

import { CandidatdeputeService } from './candidatdepute.service';

describe('CandidatdeputeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CandidatdeputeService = TestBed.get(CandidatdeputeService);
    expect(service).toBeTruthy();
  });
});
