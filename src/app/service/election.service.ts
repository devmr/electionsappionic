import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { param } from '../shared/param';

import { Election } from '../model/election';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class ElectionService {

    apiUrl:string = param.baseurl + 'api/election';

    constructor(private http: HttpClient) { }

    getAll(): Observable<Election[]> {
        return this.http.get<Election[]>(this.apiUrl)
            .pipe(
                tap(_ => this.log('fetched All')),
                catchError(this.handleError('getAll', []))
            );
    }

    getSingle(id: string) {
        return this.http.get(this.apiUrl + '/' + id);
    }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a HeroService message with the MessageService */
    private log(message: string) {
        console.log(message);
    }
}