# Election-mg - Android / IOS mobile app

Mobile app - powered by Ionic 4 of the website https://election-mg.com

## Core tools

- [Ionic](https://ionicframework.com/) v4.1
- [Angular](https://angular.io/) v7.2
- [Cordova](https://cordova.apache.org/) v7.1

### Ionic native / Cordova
- [@ionic-native/google-analytics](https://ionicframework.com/docs/native/google-analytics) - v5.6
-  @ionic/storage  - v2.2 - cordova-sqlite-storage  - v3.2

### Other Libraries tools
- [Chartjs](https://www.chartjs.org/) - v2.8
- [@ngx-translate](http://www.ngx-translate.com/) - v11.0
 

